//
//  BaseRequestAdapterTests.swift
//  APIClientTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import APIClient
import XCTest

final class BaseRequestAdapterTests: XCTestCase {

    func testAdapt() throws {
        let url = URL(string: "https://nycschools.com/")!
        let adapter = BaseRequestAdapter(baseURL: url)
        var urlRequest = URLRequest(url: URL(string: "schools/999")!)
        
        urlRequest = try adapter.adapt(urlRequest)
        
        XCTAssertEqual(urlRequest.url?.absoluteString, "https://nycschools.com/schools/999")
    }
    
    func testError() throws {
        let url = URL(string: "https://nycschools.com/")!
        let adapter = BaseRequestAdapter(baseURL: url)
        var urlRequest = URLRequest(url: URL(string: "schools/999")!)
        
        urlRequest.url = nil
        
        XCTAssertThrowsError(try adapter.adapt(urlRequest))
    }

}
