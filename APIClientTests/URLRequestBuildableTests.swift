//
//  URLRequestBuildableTests.swift
//  APIClientTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import APIClient
import XCTest

private struct URLRequestBuilder: URLRequestBuildable {}

final class URLRequestBuildableTests: XCTestCase {

    func testGet() throws {
        let url = URL(string: "https://nycschools.com")!
        let queryItems: [URLQueryItem] = [
            URLQueryItem(name: "dbn", value: "AAA"),
            URLQueryItem(name: "street", value: "Lore ave"),
        ]
        
        let urlRequest = URLRequestBuilder().get(url, queryItems: queryItems)
        
        XCTAssertEqual(urlRequest.httpMethod, "GET")
        XCTAssertEqual(urlRequest.url?.absoluteString, "https://nycschools.com?dbn=AAA&street=Lore%20ave")
    }

}
