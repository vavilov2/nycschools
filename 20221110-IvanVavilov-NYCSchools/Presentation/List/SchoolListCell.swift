//
//  SchoolListCell.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

final class SchoolListCell: TableViewCell {

    // MARK: - Subviews
    
    private let nameLabel = UILabel.make(font: .preferredFont(forTextStyle: .headline), lines: 0)
    private let addressLabel = UILabel.make(font: .preferredFont(forTextStyle: .subheadline), color: .darkGray, lines: 0)
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public methods
    
    func configure(with cellModel: SchoolListCellModel) {
        nameLabel.text = cellModel.name
        addressLabel.text = cellModel.address
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        
        nameLabel.setContentHuggingPriority(.required, for: .vertical)
        nameLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        
        addressLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        
        contentView.addSubview(nameLabel)
        contentView.addSubview(addressLabel)
        
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .defaultMargin).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.defaultMargin).isActive = true
        nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .defaultMargin).isActive = true
        
        addressLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: .defaultMargin / 4).isActive = true
        addressLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.defaultMargin).isActive = true
        addressLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .defaultMargin).isActive = true
        addressLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.defaultMargin).isActive = true
    }

}
