//
//  SchoolListCellModel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Cell Model represents dispayed data in a school list.
/// Right place to transform values from model objects to UI-friendly values.
struct SchoolListCellModel {
    
    let name: String
    let address: String
    
    init(_ school: School) {
        name = school.name
        address = [school.street, school.city, school.zip].joined(separator: ", ")
    }
    
}
