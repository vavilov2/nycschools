//
//  ViewController.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

final class SchoolListViewController: ViewController, Routing {

    typealias Story = MainRoute
    
    // MARK: - Constants
    
    private enum Constants {
        static let bottomLoadingViewHeight: CGFloat = 44
        static let bottomReloadViewHeight: CGFloat = 80
    }
    
    // MARK: - Outlets & subviews
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchBar: UISearchBar!
    private let refreshControl = UIRefreshControl()
    private let bottomLoadingView = LoadingIndicatorView()
    
    // MARK: - Private properties
    
    private let viewModel = SchoolListViewModel()
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
        showLoading()
    }
    
    // MARK: - Private methods
    
    private func setupUI() {
        refreshControl.tintColor = .systemIndigo
        refreshControl.addTarget(self, action: #selector(reload), for: .valueChanged)
        tableView.refreshControl = refreshControl
        tableView.register(SchoolListCell.self, forCellReuseIdentifier: SchoolListCell.cellID)
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func loadData() {
        Task {
            do {
                let newItems = try await viewModel.fetch()
                if newItems > 0 {
                    let count = viewModel.schools.count
                    let indexPaths = Array(count - newItems..<count)
                        .map { IndexPath(row: $0, section: 0)
                    }
                    insertRows(indexPaths)
                } else {
                    hideBottomLoading()
                }
                showNoDataIfNeeded()
            } catch {
                showError(error)
            }
            hideLoading()
        }
    }
    
    @MainActor
    private func insertRows(_ paths: [IndexPath]) {
        tableView.insertRows(at: paths, with: .automatic)
    }
    
    @objc
    private func reload() {
        Task {
            do {
                try await viewModel.reload()
                showBottomLoading()
                showNoDataIfNeeded()
                reloadData()
            } catch {
                showError(error)
            }
            endRefreshing()
        }
    }
    
    @MainActor
    private func reloadData() {
        tableView.reloadData()
    }
    
    @MainActor
    private func endRefreshing() {
        refreshControl.endRefreshing()
    }
    
    @MainActor
    private func showNoDataIfNeeded() {
        if viewModel.schools.isEmpty {
            showZeroView(with: .noData) { [weak self] in
                self?.reload()
            }
            tableView.isHidden = true
        } else {
            hideZeroView()
            tableView.isHidden = false
        }
    }
    
    @MainActor
    private func showError(_ error: Error) {
        // show zero view only if there's no data on screen to let user open loaded schools
        if viewModel.schools.isEmpty {
            tableView.isHidden = true
            let model = viewModel.zeroModel(for: error)
            showZeroView(with: model) { [weak self] in
                self?.reload()
            }
        } else {
            // Rect must be set explicitly for table view to layout correct size
            let frame = CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: Constants.bottomReloadViewHeight))
            let reloadFooterView = ReloadFooterView(frame: frame)
            let text = viewModel.errorTextForFooter(for: error)
            reloadFooterView.configure(with: text)
            reloadFooterView.onSelect = { [weak self] in
                self?.showBottomLoading()
                self?.loadData()
            }
            tableView.tableFooterView = reloadFooterView
        }
    }
    
    @MainActor
    private func showBottomLoading() {
        // Rect must be set explicitly for table view to layout correct size
        bottomLoadingView.frame = CGRect(origin: .zero, size: CGSize(width: view.bounds.width, height: Constants.bottomLoadingViewHeight))
        tableView.tableFooterView = bottomLoadingView
    }
    
    @MainActor
    private func hideBottomLoading() {
        tableView.tableFooterView = nil
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching

extension SchoolListViewController: UITableViewDataSource, UITableViewDelegate, UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.filterSchools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SchoolListCell = tableView.dequeueCell(for: indexPath)
        let cellModel = SchoolListCellModel(viewModel.filterSchools[indexPath.row])
        cell.configure(with: cellModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let isLast = indexPaths.contains { $0.row == viewModel.schools.count - 1 }
        guard isLast else { return }
        showBottomLoading()
        loadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let school = viewModel.filterSchools[indexPath.row]
        push(route: .details(school))
    }
    
}

// MARK: - UISearchBarDelegate

extension SchoolListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filter(searchText)
        reloadData()
    }
    
}
