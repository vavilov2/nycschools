//
//  SchoolListViewModel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

final class SchoolListViewModel {
    
    // MARK: - Constants
    
    private enum Constants {
        static let errorPrefix = "Couldn't load data."
        static let noInternetError = "Check your internet connection and tap to retry."
        static let badResponseError = "Something went wrong.\nTap to retry."
    }
    
    // MARK: - Public properties
    
    private(set) var schools = [School]()
    private(set) var filterSchools = [School]()
    
    // MARK: - Private properties
    
    private let service: SchoolService
    
    // MARK: - Init
    
    init(service: SchoolService = ServiceLayer.shared.schoolService) {
        self.service = service
    }
    
    // MARK: - Public methods
    
    func fetch() async throws -> Int {
        let newSchools = try await service.fetchList(offset: schools.count)
        
        if !newSchools.isEmpty {
            schools.append(contentsOf: newSchools)
            filterSchools = schools
            return newSchools.count
        } else {
            return 0
        }
    }
    
    func reload() async throws {
        schools = try await service.fetchList(offset: 0)
    }
    
    func zeroModel(for error: Error) -> ZeroModel {
        error.isNetwork ? .noInternet : .badResponse
    }
    
    func errorTextForFooter(for error: Error) -> String {
        let suffix = error.isNetwork ? Constants.noInternetError : Constants.badResponseError
        return [Constants.errorPrefix, suffix].joined(separator: " ")
    }
    
    func filter(_ text: String) {
        guard !text.isEmpty
        else {
            filterSchools = schools
            return
        }
        filterSchools = schools.filter { $0.name.contains(text) }
    }
    
}
