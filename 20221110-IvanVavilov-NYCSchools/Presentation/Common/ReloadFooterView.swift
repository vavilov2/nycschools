//
//  ReloadFooterView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 13.11.2022.
//

import UIKit

/// View used to show loading data error
final class ReloadFooterView: UIView {

    // MARK: - Public Properties
    
    var onSelect: (() -> Void)?
    
    // MARK: - Subviews
    
    private lazy var button: UIButton = {
        let action = UIAction { [weak self] _ in
            self?.onSelect?()
        }
        return UIButton(configuration: .plain(), primaryAction: action)
    }()
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public methods
    
    func configure(with text: String) {
        button.setTitle(text, for: .normal)
    }
    
    // MARK: - Private methods
    
    private func commonInit() {
        button.setTitleColor(.systemIndigo, for: .normal)
        button.setTitleColor(.systemIndigo.withAlphaComponent(0.5), for: .highlighted)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: leadingAnchor),
            button.trailingAnchor.constraint(equalTo: trailingAnchor),
            button.topAnchor.constraint(equalTo: topAnchor, constant: .defaultMargin),
            button.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -.defaultMargin)
        ])
    }

}
