//
//  UIConstants.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

extension CGFloat {
    
    static let defaultMargin: CGFloat = 16
    
}
