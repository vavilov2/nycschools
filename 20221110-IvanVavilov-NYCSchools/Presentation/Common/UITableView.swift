//
//  UITableView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

extension UITableView {

    func dequeueCell<Cell: TableViewCell>(for indexPath: IndexPath) -> Cell {
        dequeueReusableCell(withIdentifier: Cell.cellID, for: indexPath) as! Cell
    }
    
}
