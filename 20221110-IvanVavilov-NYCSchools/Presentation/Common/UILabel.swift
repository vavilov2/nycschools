//
//  UILabel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 11.11.2022.
//

import UIKit

extension UILabel {

    class func make(font: UIFont, color: UIColor = .black, lines: Int = 1, alignment: NSTextAlignment = .left) -> UILabel {
        let label = UILabel()
        label.textAlignment = alignment
        label.font = font
        label.textColor = color
        label.numberOfLines = lines
        return label
    }
    
}
