//
//  LoadingIndicatorView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 13.11.2022.
//

import UIKit

/// View with a medum style loading indicator
class LoadingIndicatorView: UIView {
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        let activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.color = .systemIndigo
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.startAnimating()
        
        addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }

}
