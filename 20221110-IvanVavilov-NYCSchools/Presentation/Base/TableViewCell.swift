//
//  TableViewCell.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

/// Base class for table view cells
class TableViewCell: UITableViewCell {
    
    /// Default cell reuse identifier equals class name
    class var cellID: String {
        String(describing: Self.self)
    }

}
