//
//  ZeroModel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 11.11.2022.
//

import UIKit

enum ErrorText {
    static let noInternet = "Check you internet connection and try again"
    static let badResponse = "Something went wrong. Please try again later"
}

struct ZeroModel {
    
    let title: String
    let subtitle: String
    let image: UIImage
    
    static let noData = ZeroModel(
        title: "No data",
        subtitle: "There is no data available at this moment. Please try again later.",
        image: UIImage(systemName: "bandage.fill")!
    )
    
    static let noInternet = ZeroModel(
        title: "No internet connection",
        subtitle: ErrorText.noInternet,
        image: UIImage(systemName: "antenna.radiowaves.left.and.right.slash")!
    )
    
    static let badResponse = ZeroModel(
        title: "Server error",
        subtitle: ErrorText.badResponse,
        image: UIImage(systemName: "xmark.icloud")!
    )
    
}
