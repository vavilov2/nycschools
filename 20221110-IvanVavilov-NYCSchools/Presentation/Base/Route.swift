//
//  Route.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

protocol Route {
    
    func makeViewController() -> UIViewController
    
}

/// Protocol for navigation from view controller
protocol Routing: UIViewController {
    
    associatedtype Story: Route
    
    func push(route: Story)
    
}

/// Default implementation for push navigation
extension Routing {
    
    func push(route: Story) {
        let vc = route.makeViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
