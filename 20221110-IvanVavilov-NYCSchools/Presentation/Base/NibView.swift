//
//  NibView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

class NibView: UIView {
    
    static var nib: UINib {
        UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func loadFromNib() -> Self {
        let results: [Any] = nib.instantiate(withOwner: self, options: nil)
        return results.compactMap { $0 as? Self }[0]
    }
    
}
