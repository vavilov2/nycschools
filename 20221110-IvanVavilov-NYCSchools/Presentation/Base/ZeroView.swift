//
//  ZeroView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 11.11.2022.
//

import UIKit

/// View used for edge cases like empty data or error
final class ZeroView: UIView {

    // MARK: - Constants
    
    private enum Constants {
        static let sideMargin: CGFloat = 64
        static let imageCenterOffset: CGFloat = -128
        static let imageHeight: CGFloat = 128
        static let buttonTitle = "Refresh"
        static let buttonHeight: CGFloat = 44
    }
    
    // MARK: - Public Properties
    
    var onRefresh: (() -> Void)?
    
    // MARK: - Subviews
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .systemIndigo
        return imageView
    }()
    private let titleLabel = UILabel.make(
        font: .preferredFont(forTextStyle: .title2),
        lines: 0,
        alignment: .center
    )
    private let subtitleLabel = UILabel.make(
        font: .preferredFont(forTextStyle: .title3),
        color: .darkGray,
        lines: 0,
        alignment: .center
    )
    private lazy var refreshButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitleColor(.systemIndigo, for: .normal)
        button.setTitleColor(.systemIndigo.withAlphaComponent(0.5), for: .highlighted)
        button.addTarget(self, action: #selector(refresh), for: .touchUpInside)
        button.setTitle(Constants.buttonTitle, for: .normal)
        return button
    }()
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    
    func configure(with model: ZeroModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        imageView.image = model.image
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subtitleLabel)
        refreshButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(refreshButton)
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: Constants.imageHeight),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: Constants.imageCenterOffset),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.sideMargin),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.sideMargin),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.sideMargin),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.sideMargin),
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: .defaultMargin),
            subtitleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.sideMargin),
            subtitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.sideMargin),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .defaultMargin),
            refreshButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.sideMargin),
            refreshButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.sideMargin),
            refreshButton.heightAnchor.constraint(equalToConstant: Constants.buttonHeight),
            refreshButton.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: .defaultMargin)
        ])
    }
    
    @objc
    private func refresh() {
        onRefresh?()
    }

}
