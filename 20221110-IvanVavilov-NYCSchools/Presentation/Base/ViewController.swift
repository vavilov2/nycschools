//
//  ViewController.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 11.11.2022.
//

import UIKit

class ViewController: UIViewController {

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.color = .systemIndigo
        return indicator
    }()
    private lazy var zeroView = ZeroView()
    
    func showLoading() {
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    @MainActor
    func hideLoading() {
        activityIndicator.isHidden = true
    }
    
    func showZeroView(with model: ZeroModel, onRefresh: @escaping () -> Void) {
        if zeroView.superview == nil {
            zeroView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(zeroView)
            NSLayoutConstraint.activate([
                zeroView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                zeroView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                zeroView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                zeroView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            ])
        }
        zeroView.onRefresh = onRefresh
        zeroView.configure(with: model)
    }
    
    func hideZeroView() {
        zeroView.removeFromSuperview()
    }

}
