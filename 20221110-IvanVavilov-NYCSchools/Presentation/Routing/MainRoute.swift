//
//  MainRoute.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

/// Main story routes
enum MainRoute: Route {
    
    /// School details
    case details(_ school: School)
    
    func makeViewController() -> UIViewController {
        switch self {
        case .details(let school):
            return SchoolDetailsViewController(school: school)
        }
    }
    
}

