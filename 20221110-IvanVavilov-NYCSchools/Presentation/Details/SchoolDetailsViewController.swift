//
//  SchoolDetailsViewController.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import UIKit

final class SchoolDetailsViewController: ViewController {

    // MARK: - Private properties
    
    private let viewModel: SchoolDetailsViewModel
    private lazy var customView = SchoolDetailsCustomView.loadFromNib()
    
    // MARK: - Init
    
    init(school: School) {
        viewModel = SchoolDetailsViewModel(school: school)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadScores()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        navigationItem.largeTitleDisplayMode = .never
        view.backgroundColor = .white
        customView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(customView)
        customView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        customView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        customView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        customView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        customView.onRefresh = { [weak self] in
            self?.loadScores()
        }
    }
    
    private func loadScores() {
        let loadingModel = viewModel.loadingModel()
        customView.configure(with: loadingModel)
        
        Task {
            let model = await viewModel.loadScores()
            configureView(with: model)
        }
    }
    
    @MainActor
    private func configureView(with model: SchoolDetailsModel) {
        customView.configure(with: model)
    }

}
