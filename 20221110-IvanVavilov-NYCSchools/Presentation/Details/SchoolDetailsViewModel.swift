//
//  SchoolDetailsViewModel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

final class SchoolDetailsViewModel {
    
    // MARK: - Private properties
    
    private let school: School
    private let service: ScoreService
    
    // MARK: - Init
    
    init(school: School, service: ScoreService = ServiceLayer.shared.scoreService) {
        self.school = school
        self.service = service
    }
    
    // MARK: - Public methods
    
    func loadScores() async -> SchoolDetailsModel {
        do {
            let score = try await service.fetchScore(for: school.dbn)
            return SchoolDetailsModel(school: school, score: score)
        } catch {
            return SchoolDetailsModel(school: school, error: error)
        }
    }
    
    func loadingModel() -> SchoolDetailsModel {
        SchoolDetailsModel(school: school, isLoading: true)
    }
    
}
