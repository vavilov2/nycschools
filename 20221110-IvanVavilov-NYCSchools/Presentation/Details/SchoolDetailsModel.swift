//
//  SchoolDetailsModel.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import CoreLocation
import MapKit

/// Model represents dispayed data in school details with avg scores.
struct SchoolDetailsModel {
    
    private enum Constants {
        static let regionBoundInMeters: Double = 2_000
        static let noScoresTitle = "Sorry, we have no scores data"
        static let someScoresTitle = "Average SAT Scores"
        static let rangeDescription = """
            \(Score.Constants.minValue) – \(Score.Constants.lowScoreBound - 1) — below average score;
            \(Score.Constants.lowScoreBound) – \(Score.Constants.highScoreBound - 1) — average score;
            \(Score.Constants.highScoreBound) – \(Score.Constants.maxValue) — higher than average.
            """
    }
    
    struct ScoreModel {
        var title: String {
            if let error = error {
                return error
            } else {
                return hideScores ? Constants.noScoresTitle : Constants.someScoresTitle
            }
        }
        let mathScore: NSAttributedString?
        let writingScore: NSAttributedString?
        let readingScore: NSAttributedString?
        let rangeDescription: String
        let isLoading: Bool
        let error: String?
        
        var hideScores: Bool {
            mathScore == nil && writingScore == nil && readingScore == nil
        }
    }
    
    let name: String
    let annotation: MKPointAnnotation?
    let region: MKCoordinateRegion?
    let address: String
    let website: String
    let scores: ScoreModel
 
    init(school: School, score: Score? = nil, isLoading: Bool = false, error: Error? = nil) {
        name = school.name
        address = [school.street, school.city, school.zip].joined(separator: ", ")
        website = school.website
        
        let errorText = error.map { $0.isNetwork ? ErrorText.noInternet : ErrorText.badResponse }
        scores = ScoreModel(
            mathScore: score?.mathAttributedString,
            writingScore: score?.writingAttributedString,
            readingScore: score?.readingAttributedString,
            rangeDescription: Constants.rangeDescription,
            isLoading: isLoading,
            error: errorText
        )
        
        if let latString = school.latitude,
            let lonString = school.longitude,
            let lat = Double(latString),
            let lon = Double(lonString) {
            let location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            region = MKCoordinateRegion(
                center: location,
                latitudinalMeters: Constants.regionBoundInMeters,
                longitudinalMeters: Constants.regionBoundInMeters
            )
            annotation = MKPointAnnotation()
            annotation?.coordinate = location
        } else {
            annotation = nil
            region = nil
        }
    }
    
}

// MARK: - Score Extension

fileprivate extension Score {
    
    /// Score bound values to consider it's low, average or high
    enum Constants {
        static let lowScoreBound = 350
        static let highScoreBound = 500
        static let minValue = 200
        static let maxValue = 800
    }
    
    var mathAttributedString: NSAttributedString? {
        guard let intMath = Int(math) else { return nil }
        return NSAttributedString(string: math, attributes: [.foregroundColor: color(for: intMath)])
    }
    
    var writingAttributedString: NSAttributedString? {
        guard let intWriting = Int(writing) else { return nil }
        return NSAttributedString(string: writing, attributes: [.foregroundColor: color(for: intWriting)])
    }
    
    var readingAttributedString: NSAttributedString? {
        guard let intReading = Int(reading) else { return nil }
        return NSAttributedString(string: reading, attributes: [.foregroundColor: color(for: intReading)])
    }
    
    private func color(for score: Int) -> UIColor {
        switch score {
        case Constants.highScoreBound...Constants.maxValue:
            return .highScoreColor
        case Constants.minValue..<Constants.lowScoreBound:
            return .lowScoreColor
        case Constants.lowScoreBound..<Constants.highScoreBound:
            return .mediumScoreColor
        default:
            // if score out of min..max bounds show it with default color
            // unless there is no requirements to hide such values or show error
            return .defaultColor
        }
    }
    
}

// MARK: - UIColor

/// Different colors low, average or high score value
private extension UIColor {
    
    static let defaultColor: UIColor = .black
    static let lowScoreColor: UIColor = .systemRed
    static let mediumScoreColor: UIColor = .systemYellow
    static let highScoreColor: UIColor = .systemGreen
    
}
