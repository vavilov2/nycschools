//
//  SchoolDetailsView.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import MapKit
import UIKit

final class SchoolDetailsCustomView: NibView {
    
    // MARK: - Public Properties
    
    var onRefresh: (() -> Void)?
    
    // MARK: - Outlets & Subviews
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var mainStack: UIStackView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var addressLabel: UILabel!
    @IBOutlet private weak var websiteTextView: UITextView!
    @IBOutlet private weak var mathScoreLabel: UILabel!
    @IBOutlet private weak var readingLabel: UILabel!
    @IBOutlet private weak var writingLabel: UILabel!
    @IBOutlet private weak var scoresTitle: UILabel!
    @IBOutlet private weak var scoresStack: UIStackView!
    @IBOutlet private weak var mathStack: UIStackView!
    @IBOutlet private weak var readingStack: UIStackView!
    @IBOutlet private weak var writingStack: UIStackView!
    @IBOutlet private weak var scoresActivityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var scoresRangesDescriptionLabel: UILabel!
    private let refreshControl = UIRefreshControl()
    
    // MARK: - View Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainStack.setCustomSpacing(.defaultMargin, after: nameLabel)
        mainStack.setCustomSpacing(.defaultMargin, after: mapView)
        scrollView.refreshControl = refreshControl
        refreshControl.tintColor = .systemIndigo
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }
    
    // MARK: - Public methods

    func configure(with model: SchoolDetailsModel) {
        refreshControl.endRefreshing()
        nameLabel.text = model.name
        addressLabel.text = model.address
        websiteTextView.text = model.website
        
        if !model.scores.hideScores {
            scoresStack.isHidden = false
            mathScoreLabel.attributedText = model.scores.mathScore
            readingLabel.attributedText = model.scores.readingScore
            writingLabel.attributedText = model.scores.writingScore
            mathStack.isHidden = model.scores.mathScore == nil
            readingStack.isHidden = model.scores.readingScore == nil
            writingStack.isHidden = model.scores.writingScore == nil
        } else {
            scoresStack.isHidden = true
        }
        scoresTitle.text = model.scores.title
        scoresTitle.isHidden = model.scores.isLoading
        scoresActivityIndicator.isHidden = !model.scores.isLoading
        scoresRangesDescriptionLabel.text = model.scores.rangeDescription
        
        if let point = model.annotation, let region = model.region {
            mapView.isHidden = false
            mapView.setRegion(region, animated: false)
            mapView.addAnnotation(point)
        } else {
            mapView.isHidden = true
        }
    }
    
    // MARK: - Private Methods
    
    @objc
    private func refresh() {
        onRefresh?()
    }
    
}
