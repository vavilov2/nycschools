//
//  Score.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

// SAT Scores information for a specific high school reflecting data from API
struct Score: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case math = "sat_math_avg_score"
        case reading = "sat_critical_reading_avg_score"
        case writing = "sat_writing_avg_score"
    }
    
    let dbn: String
    let math: String
    let reading: String
    let writing: String
    
}
