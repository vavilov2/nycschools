//
//  School.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

// High School object reflecting API entity
struct School: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case street = "primary_address_line_1"
        case city
        case zip
        case latitude
        case longitude
        case website
    }
    
    let dbn: String
    let name: String
    let street: String
    let city: String
    let zip: String
    let latitude: String?
    let longitude: String?
    let website: String
    
}
