//
//  Error.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 11.11.2022.
//

import Foundation

extension Error {
    
    var isNetwork: Bool {
        self is URLError
    }
    
}
