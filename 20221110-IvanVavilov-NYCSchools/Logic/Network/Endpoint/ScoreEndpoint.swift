//
//  ScoreEndpoint.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import APIClient

/// Endpoint for score request for a specific school
struct ScoreEndpoint: JSONEndpoint {
    
    typealias Content = [Score]
    
    let dbn: String
    
    func makeRequest() throws -> URLRequest {
        get(URL(string: "f9bf-2cp4.json")!, queryItems: [URLQueryItem(name: "dbn", value: dbn)])
    }
    
}
