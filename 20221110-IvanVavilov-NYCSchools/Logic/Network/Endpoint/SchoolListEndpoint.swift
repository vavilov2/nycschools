//
//  SchoolListEndpoint.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import APIClient
import Foundation

/// Endpoint for school list request
struct SchoolListEndpoint: JSONEndpoint {
    
    typealias Content = [School]
    
    let limit: Int
    let offset: Int
    
    func makeRequest() throws -> URLRequest {
        let parameters = [
            URLQueryItem(name: "$limit", value: String(limit)),
            URLQueryItem(name: "$offset", value: String(offset))
        ]
        return get(URL(string: "s3k6-pzi2.json")!, queryItems: parameters)
    }
    
}
