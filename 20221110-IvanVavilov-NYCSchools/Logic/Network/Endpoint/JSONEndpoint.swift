//
//  JSONEndpoint.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import APIClient

/// Helper conformance of `Endpoint` and `URLRequestBuildable` protocols to simplify endpoint implementation.
protocol JSONEndpoint: Endpoint, URLRequestBuildable where Content: Decodable {}

/// Implementation for default response decoding.
extension JSONEndpoint {
    
    var decoder: JSONDecoder {
        .default
    }

    func content(from response: URLResponse?, with body: Data) throws -> Content {
        try decoder.decode(Content.self, from: body)
    }
    
    func validate(_ request: URLRequest?, response: HTTPURLResponse, data: Data?) throws {
        // default validation
    }
}


extension JSONDecoder {

    /// Default json decoder used for api response.
    static let `default`: JSONDecoder = {
        let decoder = JSONDecoder()
        return decoder
    }()
    
}

