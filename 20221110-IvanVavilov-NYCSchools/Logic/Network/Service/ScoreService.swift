//
//  ScoreService.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Service for `Score` objects.
protocol ScoreService {
    
    /// Fetch `Score` object for a specific school
    /// - Parameter dbn: dbn filter parameter for a school
    /// - Returns: Optional `Score` object
    func fetchScore(for dbn: String) async throws -> Score?
    
}

/// Implementation of `ScoreService`.
final class ScoreServiceImpl: BaseService, ScoreService {
    
    func fetchScore(for dbn: String) async throws -> Score? {
        let endpoint = ScoreEndpoint(dbn: dbn)
        return try await apiClient.request(endpoint).first
    }
    
}

