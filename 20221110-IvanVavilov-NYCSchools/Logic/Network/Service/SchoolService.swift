//
//  SchoolService.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Service for `School` objects.
protocol SchoolService {
    
    /// Fetch list of schools.
    /// - Parameter offset: page offset
    /// - Returns: list of objects or throws an error
    func fetchList(offset: Int) async throws -> [School]
    
}

/// Implementation of `SchoolService`.
final class SchoolServiceImpl: BaseService, SchoolService {
    
    private enum Constants {
        static let limit = 20
    }
    
    func fetchList(offset: Int) async throws -> [School] {
        let endpoint = SchoolListEndpoint(limit: Constants.limit, offset: offset)
        return try await apiClient.request(endpoint)
    }
    
}
