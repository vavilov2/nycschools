//
//  ServiceLayer.swift
//  20221110-IvanVavilov-NYCSchools
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import APIClient
import Foundation

/// Singleton object that stores services
final class ServiceLayer {
    
    private enum Constants {
        static let baseURL = URL(string: "https://data.cityofnewyork.us/resource/")!
    }
    
    static let shared = ServiceLayer()
    
    let schoolService: SchoolService
    let scoreService: ScoreService
    
    private init() {
        let adapter = BaseRequestAdapter(baseURL: Constants.baseURL)
        let client = URLSessionClient(requestAdapter: adapter)
        schoolService = SchoolServiceImpl(apiClient: client)
        scoreService = ScoreServiceImpl(apiClient: client)
    }
    
}
