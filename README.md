# NYC High Schools

## Environment

Xcode 13.4.1
iOS 15.0

## Technical comments

- Transport Layer for Network is implemented in a separate framework `APIClient`.
- UI is implemented in xib/storyboard along with code only for a purpose to show skills in both approaches.
- `UITextView` used on details screen because it has built-in links detector for links.

## UI Comments

- Different SATs ranges are imaginary just to use different colors.

## Author

Ivan Vavilov

## License

No license
