//
//  ScoreTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 10.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class ScoreTests: XCTestCase {

    func testDecoding() throws {
        let json = """
        {
            "dbn": "21K728",
            "school_name": "LIBERATION DIPLOMA PLUS",
            "num_of_sat_test_takers": "10",
            "sat_critical_reading_avg_score": "411",
            "sat_math_avg_score": "369",
            "sat_writing_avg_score": "373"
        }
        """.data(using: .utf8)!
        
        let score = try JSONDecoder.default.decode(Score.self, from: json)
        
        XCTAssertEqual(score.dbn, "21K728")
        XCTAssertEqual(score.math, "369")
        XCTAssertEqual(score.writing, "373")
        XCTAssertEqual(score.reading, "411")
    }

}
