//
//  SchoolTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 10.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class SchoolTests: XCTestCase {

    private var json: Data!
    
    override func setUpWithError() throws {
        let url = Bundle(for: Self.self).url(forResource: "schoolTests", withExtension: "json")!
        json = try Data(contentsOf: url)
    }
    
    func testDecoding() throws {
        let school = try JSONDecoder.default.decode(School.self, from: json)
        
        XCTAssertEqual(school.dbn, "02M260")
        XCTAssertEqual(school.name, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(school.street, "10 East 15th Street")
        XCTAssertEqual(school.city, "Manhattan")
        XCTAssertEqual(school.latitude, "40.73653")
        XCTAssertEqual(school.longitude, "-73.9927")
        XCTAssertEqual(school.website, "www.theclintonschool.net")
    }
    
}
