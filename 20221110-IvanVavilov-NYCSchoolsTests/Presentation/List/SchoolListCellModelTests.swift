//
//  SchoolListCellModelTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class SchoolListCellModelTests: XCTestCase {

    func testInit() throws {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        
        let model = SchoolListCellModel(school)
        
        XCTAssertEqual(model.name, "My High School")
        XCTAssertEqual(model.address, "111 6th street, Brooklyn, 11222")
    }

}
