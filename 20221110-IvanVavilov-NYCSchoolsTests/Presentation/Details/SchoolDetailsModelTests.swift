//
//  SchoolDetailsModelTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class SchoolDetailsModelTests: XCTestCase {

    func testFullData() {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        let score = Score(dbn: "AAA", math: "300", reading: "400", writing: "500")
        
        let model = SchoolDetailsModel(school: school, score: score)
        
        XCTAssertEqual(model.name, "My High School")
        XCTAssertEqual(model.annotation?.coordinate.latitude, 11.1)
        XCTAssertEqual(model.annotation?.coordinate.longitude, 12.2)
        XCTAssertEqual(model.region?.center.latitude, 11.1)
        XCTAssertEqual(model.region?.center.longitude, 12.2)
        XCTAssertEqual(model.address, "111 6th street, Brooklyn, 11222")
        XCTAssertEqual(model.website, "google.com")
        XCTAssertEqual(
            model.scores.rangeDescription,
            "200 – 349 — below average score;\n350 – 499 — average score;\n500 – 800 — higher than average."
        )
        XCTAssertEqual(model.scores.mathScore?.string, "300")
        XCTAssertEqual(model.scores.readingScore?.string, "400")
        XCTAssertEqual(model.scores.writingScore?.string, "500")
        XCTAssertFalse(model.scores.hideScores)
    }
    
    func testNoScores() {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        let model = SchoolDetailsModel(school: school, score: nil)
        
        XCTAssertTrue(model.scores.hideScores)
        XCTAssertNil(model.scores.mathScore)
        XCTAssertNil(model.scores.readingScore)
        XCTAssertNil(model.scores.writingScore)
    }
    
    func testPartiallyHasScores() {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        let score = Score(dbn: "AAA", math: "300", reading: "S", writing: "-")
        
        let model = SchoolDetailsModel(school: school, score: score)
        
        XCTAssertFalse(model.scores.hideScores)
        XCTAssertEqual(model.scores.mathScore?.string, "300")
        XCTAssertNil(model.scores.readingScore)
        XCTAssertNil(model.scores.writingScore)
    }
    
    func testError() {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        
        let urlErrorModel = SchoolDetailsModel(school: school, error: URLError(.timedOut))
        let anyError = NSError(domain: "Tests", code: 100)
        let serverErrorModel = SchoolDetailsModel(school: school, error: anyError)
        
        XCTAssertEqual(urlErrorModel.scores.title, "Check you internet connection and try again")
        XCTAssertEqual(serverErrorModel.scores.title, "Something went wrong. Please try again later")
    }
    
    func testLoading() {
        let school = School(
            dbn: "AAA",
            name: "My High School",
            street: "111 6th street",
            city: "Brooklyn",
            zip: "11222",
            latitude: "11.1",
            longitude: "12.2",
            website: "google.com"
        )
        
        let model = SchoolDetailsModel(school: school, isLoading: true)
        
        XCTAssertTrue(model.scores.isLoading)
        XCTAssertTrue(model.scores.hideScores)
    }

}
