//
//  ScoreEndpointTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class ScoreEndpointTests: XCTestCase {

    func testMakeRequest() throws {
        let endpoint = ScoreEndpoint(dbn: "TERP")
        
        let request = try endpoint.makeRequest()
        
        XCTAssertEqual(request.url?.absoluteString, "f9bf-2cp4.json?dbn=TERP")
    }
    
    func testCorrectContentDecoding() throws {
        let json = """
        [{
            "dbn": "21K728",
            "school_name": "LIBERATION DIPLOMA PLUS",
            "num_of_sat_test_takers": "10",
            "sat_critical_reading_avg_score": "411",
            "sat_math_avg_score": "369",
            "sat_writing_avg_score": "373"
        }]
        """.data(using: .utf8)!
        
        let endpoint = ScoreEndpoint(dbn: "bla")
        
        let score = try endpoint.content(from: nil, with: json).first
        
        XCTAssertEqual(score?.math, "369")
        XCTAssertEqual(score?.reading, "411")
        XCTAssertEqual(score?.writing, "373")
    }
    
    func testBadContentThrowsError() {
        let json = """
        [{
            "dbn": "21K728",
            "school_name": "LIBERATION DIPLOMA PLUS",
            "num_of_sat_test_takers": "10",
            "sat_critical_reading_avg_score": "411",
            "sat_writing_avg_score": "373"
        }]
        """.data(using: .utf8)!
        
        let endpoint = ScoreEndpoint(dbn: "bla")
        
        XCTAssertThrowsError(try endpoint.content(from: nil, with: json))
    }

}
