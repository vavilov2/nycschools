//
//  SchoolListEndpointTests.swift
//  20221110-IvanVavilov-NYCSchoolsTests
//
//  Created by Ivan Vavilov on 11.11.2022.
//

@testable import _0221110_IvanVavilov_NYCSchools
import XCTest

final class SchoolListEndpointTests: XCTestCase {
    
    func testMakeRequest() throws {
        let endpoint = SchoolListEndpoint(limit: 40, offset: 10)
        
        let request = try endpoint.makeRequest()
        
        XCTAssertEqual(request.url?.absoluteString, "s3k6-pzi2.json?$limit=40&$offset=10")
    }
    
    func testCorrectContentDecoding() throws {
        let json = """
        [{
            "dbn": "02M260",
            "school_name": "Clinton School Writers & Artists, M.S. 260",
            "website": "www.theclintonschool.net",
            "primary_address_line_1": "10 East 15th Street",
            "city": "Manhattan",
            "zip": "10003",
            "latitude": "40.73653",
            "longitude": "-73.9927"
        }]
        """.data(using: .utf8)!
        
        let endpoint = SchoolListEndpoint(limit: 10, offset: 5)
        
        let school = try endpoint.content(from: nil, with: json).first
        
        XCTAssertNotNil(school)
        XCTAssertEqual(school?.dbn, "02M260")
        XCTAssertEqual(school?.name, "Clinton School Writers & Artists, M.S. 260")
        XCTAssertEqual(school?.street, "10 East 15th Street")
        XCTAssertEqual(school?.city, "Manhattan")
        XCTAssertEqual(school?.zip, "10003")
    }
    
    func testBadContentThrowsError() {
        let json = """
        [{
            "dbn": "02M260",
            "school_name": "Clinton School Writers & Artists, M.S. 260",
            "website": "www.theclintonschool.net",
            "primary_address_line_1": "10 East 15th Street",
            "city": "Manhattan"
        }]
        """.data(using: .utf8)!
        
        let endpoint = SchoolListEndpoint(limit: 100, offset: 0)
        
        XCTAssertThrowsError(try endpoint.content(from: nil, with: json))
    }

}
