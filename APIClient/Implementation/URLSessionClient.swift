//
//  URLSessionClient.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Implementation of APIClient with `URLSession`.
open class URLSessionClient: APIClient {
    
    private let session: URLSession
    private let requestAdapter: RequestAdapter
    
    public init(requestAdapter: RequestAdapter, configuration: URLSessionConfiguration = .default) {
        self.requestAdapter = requestAdapter
        self.session = URLSession(configuration: configuration)
    }
    
    public func request<T>(_ endpoint: T) async throws -> T.Content where T : Endpoint {
        var request = try endpoint.makeRequest()
        request = try requestAdapter.adapt(request)
        
        let (data, response) = try await session.data(for: request)
        if let httpResponse = response as? HTTPURLResponse {
            try endpoint.validate(request, response: httpResponse, data: data)
        }
        
        return try endpoint.content(from: response, with: data)
    }
    
}
