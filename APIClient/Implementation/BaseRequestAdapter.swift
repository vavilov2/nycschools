//
//  BaseRequestAdapter.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Base implementation for adding base URL to each request.
open class BaseRequestAdapter: RequestAdapter {
    
    private var baseURL: URL
    
    public init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    open func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        guard let url = urlRequest.url
        else {
            throw URLError(.badURL)
        }
        
        var request = urlRequest
        request.url = appendingBaseURL(to: url)
        return request
    }
    
    private func appendingBaseURL(to url: URL) -> URL {
        URL(string: url.absoluteString, relativeTo: baseURL)!
    }
}
