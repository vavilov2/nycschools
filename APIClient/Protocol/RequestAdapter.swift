//
//  RequestAdapter.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Responsible for making any changes in `URLRequest` before actually perform a request.
public protocol RequestAdapter {
    
    /// Adapts `URLRequest`
    /// - Parameter urlRequest: `URLRequest` object
    /// - Returns: adapted (changed) object
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest
    
}
