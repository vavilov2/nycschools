//
//  APIClient.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Client is responsible for performing request to a specific endpoint `Endpoint`
public protocol APIClient {
    
    /// Performs request to endpoint
    /// - Parameter endpoint: endpoint object
    /// - Returns: data of type `Endpoint.Content` if successfull, and throw an `Error` if failed.
    func request<T>(_ endpoint: T) async throws -> T.Content where T: Endpoint
    
}
