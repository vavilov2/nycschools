//
//  Endpoint.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Abstract model over specific endpoint API.
public protocol Endpoint {

    // Type of data is expected to be return if successfull
    associatedtype Content
    
    /// Creates an `URLRequest`.
    /// - Returns: `URLRequest` for current endpoint.
    func makeRequest() throws -> URLRequest

    /// Validates response before decoding.
    /// - Parameters:
    ///   - request: request object
    ///   - response: response object
    ///   - data: response data
    func validate(_ request: URLRequest?, response: HTTPURLResponse, data: Data?) throws
    
    /// Decodes the response to an expected type of data.
    /// - Parameters:
    ///   - response: response object
    ///   - body: response data
    /// - Returns: data of type `Content` of throws an error.
    func content(from response: URLResponse?, with body: Data) throws -> Content
    
}
