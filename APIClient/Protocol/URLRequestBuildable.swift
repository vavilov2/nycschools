//
//  URLRequestBuildable.swift
//  APIClient
//
//  Created by Ivan Vavilov on 10.11.2022.
//

import Foundation

/// Helper protocol for creating an `URLRequest`
public protocol URLRequestBuildable {
    
    func get(_ url: URL, queryItems: [URLQueryItem]?) -> URLRequest
    
}

/// Default implementation for get requests
public extension URLRequestBuildable {
    
    /// Creates get request with optional url-encoded query parameters.
    /// - Parameters:
    ///   - url: endpoint url
    ///   - queryItems: query parameters
    /// - Returns: `URLRequst` object
    func get(_ url: URL, queryItems: [URLQueryItem]? = nil) -> URLRequest {
        guard let queryItems = queryItems, !queryItems.isEmpty
        else {
            return URLRequest(url: url)
        }
        
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.queryItems = queryItems
        
        guard let queryURL = components?.url
        else {
            return URLRequest(url: url)
        }
        
        return URLRequest(url: queryURL)
    }
    
}
